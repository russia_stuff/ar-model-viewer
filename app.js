const express = require("express");
const path = require('path');
const hbs = require('hbs');

const app = express();


app.use(express.static(__dirname + "/public"));
app.set('views', path.join(__dirname, 'views'));
app.set("view engine", "hbs");

app.use("/", function(request, response){
    response.render("index.hbs");
});
 

console.log(__dirname)

const port = process.env.PORT || 5000;


app.listen(port, () => {
    console.log('Server started on the port: ' + port + '!');
}); 
