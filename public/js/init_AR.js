
let INIT_AR = (function () {

    let options = {
        temp: null,
        max_size: 1.0,
        renderer: null,
        scene: null,
        camera: null,
        model_group: null,
        arToolkitSource: null,
        arToolkitContext: null
    };


    let markerRoot1;


    let init = function (set) {

        options.scene = new THREE.Scene();
        options.camera = new THREE.Camera();
        options.scene.add(options.camera);

        options.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        options.renderer.setClearColor(new THREE.Color('lightgrey'), 0);

        options.renderer.setSize(set.resolution.width, set.resolution.height);

        options.renderer.domElement.style.position = 'absolute';
        options.renderer.domElement.style.top = '0px';
        options.renderer.domElement.style.left = '0px';
        document.body.appendChild(options.renderer.domElement);


        // setup arToolkitSource ----------------------------------------------------------------------------

        options.arToolkitSource = new THREEx.ArToolkitSource({
            sourceType: 'webcam',
        });

        function onResize() {
            options.arToolkitSource.onResize();
            options.arToolkitSource.copySizeTo(options.renderer.domElement);
            if (options.arToolkitContext.arController !== null) {
                options.arToolkitSource.copySizeTo(options.arToolkitContext.arController.canvas);
            }
        }

        options.arToolkitSource.init(function onReady() { onResize(); });

        // handle resize event
        window.addEventListener('resize', function () { onResize(); });


        // setup arToolkitContext ---------------------------------------------------------------------------

        // create atToolkitContext
        options.arToolkitContext = new THREEx.ArToolkitContext({
            cameraParametersUrl: 'data/camera_para.dat',
            detectionMode: 'mono'
        });

        // copy projection matrix to camera when initialization complete
        options.arToolkitContext.init(function onCompleted() {
            options.camera.projectionMatrix.copy(options.arToolkitContext.getProjectionMatrix());
        });


        // setup markerRoots --------------------------------------------------------------------------------


        // build markerControls
        markerRoot1 = new THREE.Group();
        options.scene.add(markerRoot1);
        let markerControls1 = new THREEx.ArMarkerControls(options.arToolkitContext, markerRoot1, {
            type: 'pattern', patternUrl: "data/skyciv-pattern-marker.patt",
        });


        // setup scene --------------------------------------------------------------------------------------

        options.renderer.shadowMap.enabled = true;
        options.renderer.shadowMap.type = THREE.PCFSoftShadowMap;

        let sceneGroup = new THREE.Group();
        markerRoot1.add(sceneGroup);

        // Model ---------------------------------------------------------------------------------------------

        parseData(set.scene, set.model_data);

        options.model_group = new THREE.Group();

        for (let i = 0; i < options.temp.length; i++) {
            let mesh = options.temp[i];
            mesh.name = "member";
            mesh.castShadow = true;
            mesh.receiveShadow = true;

            if (options.vertical_axis == "Y" || options.vertical_axis === undefined) {
                mesh.position.x -= options.shift_x;
                mesh.position.z -= options.shift_z;
            }
            else if (model_data.settings.vertical_axis == "Z") {
                mesh.position.x -= options.shift_x;
                mesh.position.y -= options.shift_y;
            }

            options.model_group.add(mesh);
        }
        this.temp = null;

        let scale_factor = 2 * (1 / options.max_size);

        options.model_group.scale.set(scale_factor, scale_factor, scale_factor);

        sceneGroup.add(options.model_group);

        // Floor --------------------------------------------------------------------------------------------
        let floorGeometry = new THREE.PlaneGeometry(20, 20);
        let floorMaterial = new THREE.ShadowMaterial();
        floorMaterial.opacity = 0.3;
        let floorMesh = new THREE.Mesh(floorGeometry, floorMaterial);
        floorMesh.rotation.x = -Math.PI / 2;
        floorMesh.receiveShadow = true;
        sceneGroup.add(floorMesh);


        // Light ----------------------------------------------------------------------------------------------

        let light = new THREE.PointLight(0xffffff, 1, 100);
        light.position.set(3, 3, 3); // default; light shining from top
        light.shadow.mapSize.width = 6048;
        light.shadow.mapSize.height = 6048;
        light.shadow.bias = -0.00001;
        light.shadow.radius = 1.0;
        light.shadowDarkness = 1;
        light.castShadow = true;
        sceneGroup.add(light);

        let lightSphere = new THREE.Mesh(
            new THREE.SphereGeometry(0.1),
            new THREE.MeshBasicMaterial({
                color: 0xffffff,
                transparent: true,
                opacity: 0.8
            })
        );
        lightSphere.position.copy(light.position);
        sceneGroup.add(lightSphere);



        let ambientLight = new THREE.AmbientLight(0x666666, 2.0);
        sceneGroup.add(ambientLight);
        sceneGroup.name = "model";

        // let helper = new THREE.CameraHelper( light.shadow.camera );
        // sceneGroup.add( helper );

        // UI MENU ----------------------------------------------------------------------------------------------------

        jQuery('body').append(`
        <div class="display-btn-mode ui floating dropdown icon black button"
            style="position:absolute;top:10px;left:10px;z-index:100;font-size:25px">
            <i class="cubes icon"></i>
            <div class="menu">
                <div class="ui message">
                    <div class="header">Display Settings</div>
                    <div class="ui divider"></div>
                    <!-- Rotation -->
                    <div class="ui form">
                        <label>Horizontal Rotation <span id="set-hor-deg-val">0</span> deg.</label>
                        <div class="ui range horizontal-rotation"></div>
                    </div>
                    <!-- Scaling -->
                    <div class="ui form">
                        <label>Scaling <span id="set-scaling">1</span></label>
                        <div class="ui range scaling"></div>
                    </div>
                    <!-- vertical position -->
                    <div class="ui form">
                        <label>Vertical Position <span id="set-vert-val">0</span></label>
                        <div class="ui range vertical"></div>
                    </div>
                    <div class="ui toggle checkbox shadows" style="margin-top:20px">
                        <input type="checkbox" name="public">
                        <label>Show Shadows</label>
                    </div>
                </div>
            </div>
        </div>
        `);

        jQuery(".display-btn-mode").dropdown({ transition: 'drop' }).dropdown({ on: 'hover' });

        // UI functions
        jQuery('.horizontal-rotation').range({
            min: 0,
            max: 360,
            start: 0,
            onChange: function (val) {
                // console.log(val + 1)
                jQuery("#set-hor-deg-val").html(val);
                options.scene.children[1].children[0].rotation.y = THREE.Math.degToRad(val + 1);
            }
        });

        jQuery('.scaling').range({
            min: 1,
            max: 10,
            start: 1,
            onChange: function (val) {
                // console.log(val + 1)
                jQuery("#set-scaling").html(val);
                options.scene.children[1].children[0].scale.set(val, val, val);
            }
        });

        jQuery('.vertical').range({
            min: 0,
            max: 5,
            start: 0,
            step: 0.25,
            onChange: function (val) {
                // console.log(val + 1)
                jQuery("#set-vert-val").html(val);
                options.scene.children[1].children[0].position.y = val;
            }
        });

        jQuery(".shadows").checkbox('set checked');
        jQuery(".shadows").checkbox({
            onChecked: function () { showShadows(true); },
            onUnchecked: function () { showShadows(false); }
        });

        animate();

    };

    function animate() {

        requestAnimationFrame(animate);
        if (options.arToolkitSource.ready !== false) options.arToolkitContext.update(options.arToolkitSource.domElement);
        options.renderer.render(options.scene, options.camera);
    }


    let showShadows = function (shadow) {

        for (let i = 0; i < options.scene.children[1].children[0].children[0].children.length; i++) {
            let mesh = options.scene.children[1].children[0].children[0].children[i];
            mesh.castShadow = shadow;
            mesh.receiveShadow = shadow;
        }
    };

    let parseData = function (scene, model_data) {

        let x_arr = [];
        let y_arr = [];
        let z_arr = [];

        for (let node in model_data.nodes) {
            x_arr.push(model_data.nodes[node].x);
            y_arr.push(model_data.nodes[node].y);
            z_arr.push(model_data.nodes[node].z);
        }

        let max_x = _.max(x_arr);
        let min_x = _.min(x_arr);
        let max_y = _.max(y_arr);
        let min_y = _.min(y_arr);
        let max_z = _.max(z_arr);
        let min_z = _.min(z_arr);

        INIT_AR.options.max_size = Math.max(Math.abs(max_x - min_x), Math.abs(max_y - min_y), Math.abs(max_z - min_z));

        let delta_x = max_x - min_x;
        let delta_y = max_y - min_y;
        let delta_z = max_z - min_z;

        let shift_x = max_x - 0.5 * delta_x;
        let shift_y = max_y - 0.5 * delta_y;
        let shift_z = max_z - 0.5 * delta_z;

        INIT_AR.options.vertical_axis = model_data.settings.vertical_axis;
        INIT_AR.options.shift_x = shift_x;
        INIT_AR.options.shift_y = shift_y;
        INIT_AR.options.shift_z = shift_z;

        options.temp = [];
        for (let i = 0; i < scene.children.length; i++) {
            if (scene.children[i].type == "Mesh") options.temp.push(scene.children[i]);
        }
    };


    return {
        init: init,
        options: options,
    };


})(); 