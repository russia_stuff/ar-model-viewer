


var S3D_MODEL = (function () {

    let viewer = {};
    let model_data = {};


    let getModelData = function () {

        var auth_username = "michael@skyciv.com";
        var auth_token_key = "akWaLTynYjPE7Ga8SNB34T8PhdD4gq1Wv3cghFOYO7BHwrQ1HNQJ78A5JU036AWr";
        var api_object = {
            auth: {
                username: auth_username,
                key: auth_token_key
            },
            functions: [
                {
                    "function": "S3D.session.start",
                    "arguments": {
                        "keep_open": false
                    }
                },
                {
                    "function": "S3D.file.open",
                    "arguments": {
                        "name": "AR TEST FOLDER/arch_bridge",
                        "path": ""
                    }
                },
                {
                    "function": "S3D.model.get",
                    "arguments": {}
                },
            ]
        };

        jQuery.ajax({
            url: "https://api.skyciv.com:8085/v3",
            data: JSON.stringify(api_object),
            type: "POST",
            timeout: parseInt(1000 * 60 * 0.5), // 0.5 minutes
            success: function (data) {

                S3D_MODEL.model_data = data.response.data;

                S3D_MODEL.viewer = new SKYCIV.renderer({ container_selector: '#test', });
                S3D_MODEL.viewer.model.set(S3D_MODEL.model_data);
                let sett = S3D_MODEL.viewer.settings.get();
                sett.visibility.global_axis = false;
                S3D_MODEL.viewer.model.buildStructure();
                S3D_MODEL.viewer.render();

                let scene = S3D_MODEL.viewer.getThreeJsVar('scene');

                jQuery("#loader-cover").css("display", "none");

                jQuery("body").append('<button id="display-ar-btn" class="ui primary button" style="position:absolute;right:30px;bottom:30px;"> <h1>AR</h1></button>');


                // 
                // This is the starting point for displaying the model in AR mode
                //
                jQuery("#display-ar-btn").click(function () {

                    jQuery("body").empty();
                    let setting = {
                        resolution: { width: 720, height: 480 },
                        scene: scene,
                        model_data: S3D_MODEL.model_data
                    };
                    INIT_AR.init(setting);
                });

            }

        });

    };


    return {
        getModelData: getModelData,
        model_data: model_data,
        viewer: viewer,
    };


})();